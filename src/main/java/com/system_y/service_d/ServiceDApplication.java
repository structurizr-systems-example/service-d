package com.system_y.service_d;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ServiceDApplication {

	public static void main(String[] args) {
		SpringApplication.run(ServiceDApplication.class, args);
	}
}
